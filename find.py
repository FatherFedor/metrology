import re
from collections import Counter

def delete_rubbish(source):
	source = re.sub(r'\".*?\"', '', source, flags = re.DOTALL)
	source = re.sub(r'//.*', '', source)
	source = re.sub(r'\/\*.*?\*\/', '', source, flags = re.DOTALL)		
	return source

def count_conope(source):
	number_case = len(re.findall(r'\bcase\b', source))
	if (number_case > 0):
		number_case -= 1

	number = len(re.findall(r'(\bif\b|\bwhile\b|\bfor\b|\bdo\b|\?)', source))
	return number + number_case

def count_operator(source):
	operators = r'(delete|new|typeid|alignof|sizeof|::|>>=|<<=|==|>=|<=|&=|\|=|!=|\|\||\+=|\*=|\-=|/=|%=|\^=|>>|<<|->|&&|\+\+|--|=|\+|-|\*|/|%|<|>|!|&|\||\^|~|\.|\?|::)'
	number = 0
	number = len(re.findall(operators, source))	  
	return number	

def find_nesting(source, start, end, nest_level = -1, max_level = -1):
	i = start
	balance_brace = 0
	balance_parenthesis = 0
	number_que = 0
	while (i < end):
		if (source[i:(i+2)] == 'if') or (source[i:(i+3)] == 'for') or (source[i:(i+4)] == 'while') or (source[i:(i+2)] == 'do') or source[i:i+4] == 'else' :
			nest_level += 1;
			max_level = max(nest_level, max_level)
			balance_brace = 0
			balance_parenthesis = 0
			start = i
			while not (((source[i] == '}') or (source[i] == ';')) and (balance_brace == 0) and (balance_parenthesis == 0)):
				i += 1
				if source[i] == '(':
					balance_brace += 1
				if source[i] == ')':
					balance_brace -= 1
				if source[i] == '{':
					balance_brace += 1
				if source[i] == '}':
					balance_brace -= 1
			print('\n\n\n')			
			print(source[start:i+1])
			print(nest_level)
			print(max_level)
			max_level = find_nesting(source, start+1, i+1, nest_level, max_level)
			nest_level -= 1
		if source[i:(i+6)] == 'switch':
			number_case = 0
			
			while not ((source[i] == '}') and (balance_parenthesis == 0)):
				i += 1
				nest_level -= 1
				if source[i] == '{':
					balance_brace += 1
				if source[i] == '}':
					balance_brace -= 1
				if (source[i:(i+4)] == 'case'):
					start = i
					nest_level += 1
					number_case += 1
					max_level = max(nest_level, max_level)
					while not source[i:i+6] == 'break;' and (balance_parenthesis == 0):
						i += 1
					if source[i] == '{':
						balance_brace += 1
					if source[i] == '}':
						balance_brace -= 1
					print('\n\n\n')
					print(source[start:i])
					print(nest_level)
					print(max_level)
					max_level = find_nesting(source, start+1, i, nest_level, max_level)
				if (source[i:(i+7)] == 'default'):
					start = i
					max_level = max(nest_level, max_level)
					while not source[i] == '}' and balance_parenthesis == 0:
						i += 1
						if source[i] == '{':
							balance_brace += 1
						if source[i] == '}':
							balance_brace -= 1
					print('\n\n\n')
					print(source[start:i])
					print(nest_level)
					print(max_level)
					max_level = find_nesting(source, start+1, i, nest_level, max_level)
			nest_level -= number_case
		if source[i] == '?':
			while source[i] != '\n':
				i += 1
				print(source[i])
				if source[i] == '?': 
					number_que += 1 
			nest_level += number_que
			max_level = max(nest_level, max_level)
			nest_level -= number_que
			number_que = 0
		i += 1
	return max_level

def count_max_condit(source):	
	return find_nesting(source, 0, len(source)-1)
	
def count_spen(source):
	key_words = ('alignas','alignof','and','and_eq','asm','auto','bitand','bitor','bool','break','case','catch','char','char16_t','char32_t','class','compl','const','constexpr','const_cast','continue','decltype','default','delete','do','double','dynamic_cast','else','enum','explicit','export','extern','false','float','for','friend','goto','if','inline','int','long','mutable','namespace','new','noexcept','not','not_eq','nullptr','operator','or','or_eq','private','protected','public','register','reinterpret_cast','return','short','signed','sizeof','static','static_assert','static_cast','struct','switch','template','this','thread_local','throw','true','try','typedef','typeid','typename','union','unsigned','using','virtual','void','volatile','wchar_t','while', 'xor', 'xor_eq')
	# words = re.findall(r'([a-zA-Z_]+)', source)
	# for key_word in key_words:
	# 	for word in words:
	# 		if key_word == word:
	# 			words.remove(word)
	# for key_word in key_words:
	# 	for word in words:
	# 		if key_word == word:
	# 			words.remove(word)	
	_words = re.findall(r'(int|bool|char|string|char16_t|char32_t|float|double|long) ([a-zA-Z_1-9]+)', source)
	var = []
	words = []
	for w in _words:
	 	var.append(w[1])
	d = list(Counter(var))
	for v in d:
		words += re.findall(r'\b' + v + r'\b', source)

	dic = Counter(words)
	print(dic)
	l = len(dic)
	summ = sum(dic.values())
	return summ - l 

def count_chepin(source):
	var = re.findall(r'(int|bool|char|string|char16_t|char32_t|float|double|long) ([a-zA-Z_1-9]+)', source)
	_var = []
	for st in var:		
		_var.append(st[1])
	input_var = re.findall(r'cin>>([a-zA-Z_1-9]+)', source)
	r_input_var = re.findall(r'scanf\((.*?)\)', source, re.DOTALL)
	for r in r_input_var:
		for variable in _var:
			if (len(re.findall(r'\b&?' + variable + r'\b', r)) > 0):
				input_var.append(variable)
	output_var = re.findall(r'<<([a-zA-Z_&1-9]+)<<', source)
	r_output_var = re.findall(r'printf\((.*?)\)', source, re.DOTALL)
	for r in r_output_var:
		for variable in _var:
			if (len(re.findall(r'\b' + variable + r'\b', r)) > 0):
				output_var.append(variable)
	change_var = []
	for variable in _var:
		new = '(' + variable + ')' +' [=]'	
		change_var += re.findall(new, source)
	for variable in _var:
		new = '(' + variable + ')' +'\+\+'	
		change_var += re.findall(new, source)
	for variable in _var:
		new = '(' + variable + ')' + r'\[\]' + ' ='	
		change_var += re.findall(new, source)
	_change_var = []
	temp = Counter(change_var)
	_change_var = list(temp)
	manage_var = []
	cond = re.findall(r'(\bif\b|\bwhile\b|\bfor\b|\bdo\b|\bswitch\b)( \(.*\))', source)
	str_cond = []
	for st in cond:		
		str_cond.append(st[1])
	for s in str_cond:
		for variable in _var:
			new = r'\b' + variable + r'\b'
			if (len(re.findall(new, s)) != 0):
				manage_var.append(variable)	
	temp = Counter(manage_var)
	manage_var = list(temp)
	P = list(set(input_var).difference(change_var))
	T = list(set(_var).difference(output_var + change_var + input_var))
	M = list(set(change_var).difference(manage_var))
	print('P',P)
	print('M',M)
	print('C',manage_var)
	print('T',T)
	p = list(set(input_var).difference(change_var))
	m = list(set(input_var + output_var).difference(manage_var + p))
	c = list(set(manage_var).intersection(input_var + output_var))
	#print(p)
	#print(m)
	#print(c)
	#t = list(set)
	# return (1*len(p) + 2*len(m) + 3*len(c))
	return (1*len(P) + 2*len(M) + 3*len(manage_var) + 0.5*len(T))

def reader(filename):
	with open(filename, 'r') as f:
		code = f.read()
		code = delete_rubbish(code)
		# number_of_conope = count_conope(code)
		# number_of_operator = number_of_conope + count_operator(code)
		#number_max_condit = count_max_condit(code)
		sum_spen = count_spen(code)
		chepin = count_chepin(code)
		#print(number_of_conope)
		#print(number_of_operator)

	#print('CL - absolute complexity of the program {0}'.format(number_of_conope))
	#print('cl - relative complexity of the program {:f}'.format((number_of_conope*1.0) / (number_of_operator*1.0)))
	#print('the maximum level of nesting {0}'.format(number_max_condit))
	print('total spen in programm {0}'.format(sum_spen))
	print('Chepin full',chepin)
	#print(code)

if __name__ == '__main__':
	reader('source1.cpp')